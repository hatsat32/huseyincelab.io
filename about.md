---
layout: page
title: Hakkımda
permalink: /about
---

Ben **Hüseyin Altunkaynak**. Siber güvenlik ile uğraşmayı seviyorum. Kişisel blog sayfamdır.

Bana sormak istediğiniz bir soru olursa, aşağıda bana ulaşabileceğiniz bazı bağlantılar mevcut:

Email [huseyin.altunkaynak51@gmail.com](mailto:huseyin.altunkaynak51@gmail.com)

Hack The Box
<script src="https://www.hackthebox.eu/badge/52437"></script>